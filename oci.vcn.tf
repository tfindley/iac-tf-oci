resource "oci_core_vcn" "new_vcn" {
  dns_label      = var.vcn_dns_label
  cidr_block     = var.vcn_cidr_block
  compartment_id = var.OCI_COMPARTMENT
  display_name   = var.vcn_display_name
}
# Build Oracle Cloud Infrastructure using Terraform

Infrastrucutre as Code implmenetation using Hashicorp Terraform, deploying into Oracle Cloud Infrastructure.

**NOTE:** This implementation is designed to work against the Always Free tier of OCI (it will also work against the paid tier), however you are responsiblef or any costs you may incur for running this.

## Requirements

In order to utilise this you will need an Oracle Cloud Infrastucture account. You can sign up for a free account [here](https://www.oracle.com/uk/cloud/free/).

Once you have your account, you will need to generate an API RSA Key, download the private key file and take note of the key Fingerprint, your User OCID and your Tenancy OCID. The best way to utilise these is by setting them in your environmental variables file. See below for details on these and help on where to obtain / generate them.

You will also need the following:
- [Terraform](https://www.terraform.io/downloads)
- [Terraform OCI Provider](https://registry.terraform.io/providers/oracle/oci)
- [Oracle Cloud Shell (optional)]()

A technical Getting Started guide from Oracle is available [here](https://docs.oracle.com/en-us/iaas/Content/API/SDKDocs/terraform.htm).

A 'good' Getting Started Tutorial from Oracle is availalbe [here](https://docs.oracle.com/en-us/iaas/developer-tutorials/tutorials/tf-provider/01-summary.htm)

### Environmental Variables

Remember: all environmental variables accessed by Terraform must start with TF_VAR_ and be declared as a variable inside the Terraform code. 

|                   **Variable** 	| **Description**                         	| **Example**                               	| **Help** 	|
|-------------------------------:	|-----------------------------------------	|-------------------------------------------	|----------	|
|           TF_VAR_OCI_USER_OCID 	| Oracle Cloud User ID                    	| ocid1.user.oc1..aaaabbbbccc3213etc        	| [Link](https://docs.oracle.com/en-us/iaas/Content/API/Concepts/apisigningkey.htm#five)                 	|
|        TF_VAR_OCI_TENANCY_OCID 	| Oracle Cloud Tenancy ID                 	| ocid1.tenancy.oc1..aaaabbbbccc3213etc     	| [Link](https://docs.oracle.com/en-us/iaas/Content/API/Concepts/apisigningkey.htm#five)                 	|
|         TF_VAR_OCI_COMPARTMENT 	| Oracle Cloud Compartment                	| ocid1.compartment.oc1..aaaabbbbccc3213etc 	| [Link](https://docs.oracle.com/en-us/iaas/developer-tutorials/tutorials/tf-compartment/01-summary.htm) 	|
|              TF_VAR_OCI_REGION 	| Oracle Cloud Deployment region          	| "us-ashburn-1"                            	| [Link](https://docs.oracle.com/en-us/iaas/Content/Identity/Tasks/managingregions.htm)                  	|
|        TF_VAR_OCI_PRIVKEY_PATH 	| Path to your OCI API Private Key        	| "~/.oci/privkey.pem"                      	| [Link](https://docs.oracle.com/en-us/iaas/Content/API/Concepts/apisigningkey.htm#two)                  	|
| TF_VAR_OCI_PRIVKEY_FINGERPRINT 	| Fingerprint of your OCI API Private Key 	| aa:bb:cc:dd:ee:ff:00:11:22:33:44:55       	| [Link](https://docs.oracle.com/en-us/iaas/Content/API/Concepts/apisigningkey.htm#four)                 	|

## Terraform Backend - OCI S3 configuration

If yo would like to store your .state files in an OCI S3 bucket:

### Create your S3 bucket

#### Manual method

- Log on to your OCI instance
- Go to Storage > Buckets
- Choose 'Create Bucket'
  - Bucket Name: something like 'tfstatestore' (remember this bucket name - you need it your backend configuration)
  - Storage Tier: Standard
  - Enable Object Versioning: True
  - leave everything else as default or set as you require.
- Now that your bucket is created, note down your Namespace - it should be a randomly generated string of letters and numbers

#### IAC (Terraform) method

You can use the following Terraform code to generate an S3 bucket:

      data "oci_objectstorage_namespace" "getnamespace" {
        #Optional
        compartment_id = var.OCI_COMPARTMENT
      }

      resource "oci_objectstorage_bucket" "tfstatestore" {
        #Required
        compartment_id = var.OCI_COMPARTMENT
        name           = "tfstatestore"
        namespace      = data.oci_objectstorage_namespace.getnamespace.namespace

        #Optional
        auto_tiering = "InfrequentAccess"
        storage_tier          = "Standard"
        versioning = "Enabled"
      }

      output "bucket_name" {
        description = "Your bucket name - needed for the terraform backend configuration"
        value = oci_objectstorage_bucket.tfstatestore.name
      }

      output "bucket_namespace" {
        description = "Your bucket namespace - needed for the AWS S3 Endpoint URL"
        value = oci_objectstorage_bucket.tfstatestore.namespace
      }

### Configure your backend

- Create a file named (something like): backend.tf (NOTE: backend.tf is set as ignored in .gitignore)
- Add the following code:

      terraform {
        backend "s3" {
        bucket   = "<bucket_name>"
        key      = "<unique_key>.tfstate"
        skip_region_validation      = true
        skip_credentials_validation = true
        skip_metadata_api_check     = true
        force_path_style            = true
        }
      }

### Configure your environment

Set the following Environmental Variables:

|                   **Variable** 	| **Description**                         	| **Example**                                                                  	| **Help** 	|
|-------------------------------:	|-----------------------------------------	|----------------------------------------------------------------------------- 	|-------------------------------------------------------------------	|
|                    AWS_REGION 	| Oracle Cloud User ID                    	| "us-ashburn-1"                                                               	| [Link](https://docs.oracle.com/en-us/iaas/Content/Identity/Tasks/managingregions.htm) |
|             AWS_ACCESS_KEY_ID 	| Oracle Cloud Tenancy ID                 	| 1234abcd15678ed90                                                            	| Profile > My Profile > Customer Secret Keys > Access Key > Copy     |
|         AWS_SECRET_ACCESS_KEY 	| Oracle Cloud Compartment                	| abc123def456ghi789etc                                                        	| Generate a new key in Profile > My Profile > Customer Secret Keys 	|
|               AWS_S3_ENDPOINT 	| Oracle Cloud Deployment region          	| https://__bucket-namespace__.compat.objectstorage.__oci-region__.oraclecloud.com 	| Get your bucket-namespace from Object Storage > Bucket Details > Namespace |

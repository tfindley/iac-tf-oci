resource "oci_core_subnet" "new_subnet" {
  vcn_id                     = oci_core_vcn.new_vcn.id
  cidr_block                 = var.subnet_cidr_block
  compartment_id             = var.OCI_COMPARTMENT
  display_name               = var.subnet_display_name
  prohibit_public_ip_on_vnic = var.subnet_prohibit_public
  dns_label                  = var.subnet_dns_label
}
# ------------------------
# Build stage
# ------------------------

resource "oci_core_instance" "tf_compute" {
  # Required
  availability_domain = data.oci_identity_availability_domains.test_availability_domains.availability_domains[0].name
  # availability_domain = data.oci_identity_availability_domains.ads.availability_domains[1].name
  compartment_id = var.OCI_COMPARTMENT
  # shape               = data.oci_core_shapes.test_shapes.id
  shape = var.compute_shape

  source_details {
    source_id   = data.oci_core_images.oci_core_image_lookup.images.0.id
    source_type = "image"
  }

  # Optional
  display_name = var.compute_name

  shape_config {
    ocpus         = var.compute_cpus
    memory_in_gbs = var.compute_memory_in_gbs
  }

  create_vnic_details {
    subnet_id        = oci_core_subnet.new_subnet.id
    assign_public_ip = true
  }

  metadata = {
    ssh_authorized_keys = file(var.compute_ssh_authorized_keys)
  }

  preserve_boot_volume = false
}



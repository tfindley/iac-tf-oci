# ------------------------
# Environmental variables
# ------------------------
# These must be set in your environment and must be prefixed with TF_VAR_
# e.g: OCI_COMPARTMENT == TF_VAR_OCI_COMPARTMENT

# OCI Variables

variable "OCI_COMPARTMENT" {
  type = string
}

variable "OCI_TENANCY_OCID" {
  type = string
}

variable "OCI_USER_OCID" {
  type = string
}

variable "OCI_PRIVKEY_FINGERPRINT" {
  type = string
}

variable "OCI_PRIVKEY_PATH" {
  type = string
}

variable "OCI_REGION" {
  type    = string
  default = "us-phoenix-1"
}

# ------------------------
# Static Input Variables
# ------------------------

# Compute variables
variable "compute_name" {
  type = string
  # default = "tf_test"
}

variable "compute_subnet_id" {
  type = string
  # default = ""
}

variable "compute_ssh_authorized_keys" {
  type = string
  # default = "~/.ssh/id_rsa.pub"
}

variable "compute_shape" {
  type = string
  # default = "VM.Standard.E2.1.Micro"
}

variable "compute_cpus" {
  type = string
  # default = "1"
}

variable "compute_memory_in_gbs" {
  type = string
  # default = "1"
}


# Subnet variables
variable "subnet_cidr_block" {
  type = string
  default = "172.16.0.0/24"
}

variable "subnet_display_name" {
  type = string
  default = "New Private Terraform subnet"
}

variable "subnet_prohibit_public" {
  type = bool
  default = true
}

variable "subnet_dns_label" {
  type = string
  default = "privdev"
}

variable "subnet_igw_displayname" {
  type = string
  default = "pubdev_igw"
}

variable "subnet_igw_rt_displayname" {
  type = string
  default = "pubdev_igw_route"
}


# VCN Variables
variable "vcn_dns_label" {
  type = string
  default = "new_tf_vcn"
}

variable "vcn_cidr_block" {
  type = string
  default = "172.16.0.0/20"
}

variable "vcn_display_name" {
  type = string
  default = "New Terraform VCN"
}
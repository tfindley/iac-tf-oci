resource "oci_core_internet_gateway" "pubdev_internet_gateway" {
  #Required
  compartment_id = var.OCI_COMPARTMENT
  vcn_id         = oci_core_vcn.new_vcn.id

  #Optional
  display_name = var.subnet_igw_displayname
}

resource "oci_core_default_route_table" "pubdev_igw_route_table" {
  #Required
  manage_default_resource_id = oci_core_vcn.new_vcn.default_route_table_id

  #Optional
  display_name = var.subnet_igw_rt_displayname
  route_rules {
    #Required
    network_entity_id = oci_core_internet_gateway.pubdev_internet_gateway.id

    #Optional
    # cidr_block = var.route_table_route_rules_cidr_block
    # description = var.route_table_route_rules_description
    # destination = var.route_table_route_rules_destination
    destination = "0.0.0.0/0"
    # destination_type = var.route_table_route_rules_destination_type
  }
}
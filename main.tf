terraform {
  required_providers {
    oci = {
      source = "oracle/oci"
    }
  }
}

provider "oci" {
  region = var.OCI_REGION

  auth             = "APIKey"
  tenancy_ocid     = var.OCI_TENANCY_OCID
  user_ocid        = var.OCI_USER_OCID
  fingerprint      = var.OCI_PRIVKEY_FINGERPRINT
  private_key_path = var.OCI_PRIVKEY_PATH
}
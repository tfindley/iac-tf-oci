# OCI Core
output "oci_availability_domain" {
  value = resource.oci_core_instance.tf_compute.availability_domain
}

# OCI Subnet
output "network_subnet_prefix" {
  value = resource.oci_core_subnet.new_subnet.cidr_block
}


# OCI Compute
output "compute_id" {
  value = oci_core_instance.tf_compute.id
}

output "db_state" {
  value = oci_core_instance.tf_compute.state
}

output "compute_public_ip" {
  value = oci_core_instance.tf_compute.public_ip
}

output "compute_disk_size_gb" {
  value = resource.oci_core_instance.tf_compute.source_details[0].boot_volume_size_in_gbs
}

output "compute_mem_size_gb" {
  value = resource.oci_core_instance.tf_compute.shape_config[0].memory_in_gbs
}

output "compute_ocpus" {
  value = resource.oci_core_instance.tf_compute.shape_config[0].ocpus
}

output "compute_hostname" {
  value = resource.oci_core_instance.tf_compute.hostname_label
}

output "compute_nic_name" {
  value = resource.oci_core_instance.tf_compute.create_vnic_details[0].display_name
}

output "compute_nic_external" {
  value = resource.oci_core_instance.tf_compute.public_ip
}

output "compute_nic_ipv4" {
  value = resource.oci_core_instance.tf_compute.create_vnic_details[0].private_ip
}
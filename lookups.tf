# ------------------------
# Lookups
# ------------------------

# Lookup the availability domain for the compute instance
data "oci_identity_availability_domains" "test_availability_domains" {
  #Required
  compartment_id = var.OCI_COMPARTMENT
  filter {
    name   = "name"
    values = ["[a-zA-Z]:PHX-AD-2"] # tsBj:PHX-AD-2
    regex  = true
  }
}

output "test_availability_domains_list" {
  value = data.oci_identity_availability_domains.test_availability_domains.availability_domains
}

# ------------------------

# Lookup the image we want to install (in this case, Ubuntu 22.04 Minimal, latest version)
data "oci_core_images" "oci_core_image_lookup" {
  compartment_id   = var.OCI_COMPARTMENT
  operating_system = "Canonical Ubuntu"
  filter {
    name   = "display_name"
    values = ["^Canonical-Ubuntu-22.04-Minimal-([\\.0-9-]+)$"]
    regex  = true
  }
}

output "oci_core_image_lookup-latest-name" {
  value = data.oci_core_images.oci_core_image_lookup.images.0.display_name
}

output "oci_core_image_lookup-latest-id" {
  value = data.oci_core_images.oci_core_image_lookup.images.0.id
}

# ------------------------

# Lookup the compute instance shape templates os we can clone that.
# In this instance we check what instances are compatible with our Image
data "oci_core_shapes" "get_shapes" {
  compartment_id      = var.OCI_COMPARTMENT
  availability_domain = data.oci_identity_availability_domains.test_availability_domains.availability_domains.0.name
  image_id            = data.oci_core_images.oci_core_image_lookup.images.0.id
  filter {
    name   = "billing_type"
    values = ["ALWAYS_FREE"]
    regex  = true
  }
}

output "get_shapes_list" {
  value = data.oci_core_shapes.get_shapes.shapes.0.name
}
